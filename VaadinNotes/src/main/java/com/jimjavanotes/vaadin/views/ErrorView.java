package com.jimjavanotes.vaadin.views;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("serial")
public class ErrorView extends VerticalLayout implements View {

	public ErrorView () {
 addComponent (new Label ("error"));
 }
 @Override
 public void enter(ViewChangeEvent event) {}
}