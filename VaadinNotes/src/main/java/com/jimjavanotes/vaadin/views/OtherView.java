package com.jimjavanotes.vaadin.views;

import javax.annotation.PostConstruct;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("serial")
@SpringView(name = "other")
public class OtherView extends VerticalLayout implements View {

	@PostConstruct
	void init() {
		addComponent(new Label("other"));
	}

	@Override
	public void enter(ViewChangeEvent event) {
	}
}