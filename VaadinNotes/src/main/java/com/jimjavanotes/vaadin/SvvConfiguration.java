package com.jimjavanotes.vaadin;

import org.springframework.context.annotation.Configuration;

import com.vaadin.spring.annotation.EnableVaadin;

@Configuration
@EnableVaadin
public class SvvConfiguration {
}