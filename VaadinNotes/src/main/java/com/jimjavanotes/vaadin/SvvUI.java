package com.jimjavanotes.vaadin;

import org.springframework.beans.factory.annotation.Autowired;

import com.jimjavanotes.vaadin.views.ErrorView;
import com.vaadin.navigator.Navigator;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.spring.navigator.SpringViewProvider;
import com.vaadin.ui.Panel;
import com.vaadin.ui.UI;

@SuppressWarnings("serial")
@SpringUI
public class SvvUI extends UI {

	private final SpringViewProvider viewProvider;

	@Autowired
	public SvvUI(SpringViewProvider viewProvider) {
		this.viewProvider = viewProvider;
	}

	@Override
	protected void init(VaadinRequest request) {
		final Panel viewContainer = new Panel();
		viewContainer.setSizeFull();

		setContent(viewContainer);

		Navigator navigator = new Navigator(this, viewContainer);
		navigator.setErrorView(new ErrorView());
		navigator.addProvider(viewProvider);
	}
}